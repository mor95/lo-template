/* ¡No modificar! */

'use strict'
const $                    = require('jquery')
const animatePerSlide      = require('animate-per-slide')
const ovaProgress           = require('lo-progress')
const stageResize          = require('stage-resize')
const presentation         = require('presentation')
const ovaConcepts          = require('lo-concepts')
const _                    = require('lodash')
const Hammer               = require('hammerjs')
const courseData           = require('courseData.json')
const concepts             = require('concepts.json')
const isMobile             = require('is-mobile')
const preciseDraggable     = require('precise-draggable')
const ovaNavigationButtons = require('ova-navigation-buttons')
const ovaTeam               = require('lo-team')
const swal                 = require('sweetalert2')
const swalOptions          = require('swalOptions.js')
const loAPI                = require('lo-api')
const ovaPreload           = require('ova-preload')
const loTopicsFunctions   = require('lo-topics-functions')
const ovaConfig            = require('ovaConfig.json')
const imageSources         = require('imageSources.json')
const fontSources          = require('fontSources.json')
const topics               = require('topics.json')
const preloadSpinner       = require('preloadSpinner.js')
const staticTemplates      = require('staticTemplates.js')
const slideHooks           = require('slideHooks.js')
const utils                = require('utils.js')
const ovaCourseActivities  = require('lo-course-activities')
const finishOva            = require('finishOva.js')
const ovaTooltip           = require('ova-tooltip')

const app = function() {
    var self = this
    self.APIInstance = new loAPI({
        remoteURL: courseData.remoteURL
    })
    self.ovaPreload = new ovaPreload()
    self.ovaProgress = new ovaProgress({
        courseData: courseData,
        topics: topics,
        APIInstance: self.APIInstance,
    })

    preloadSpinner.updateMessage('Escalando escenario...')
    
    //Se escala el escenario al finalizar la carga de la ventana
    stageResize.resizeStage()
    
    //Se añaden eventos de auto-escala en evento de redimensionamiento en ventana
    stageResize.addWindowResizeEvents()

    //Algunos slides requieren el estado de esta variable
    //Se renderizan los templates en \partials
    staticTemplates.renderPartials(courseData)

    self.ovaConcepts = new ovaConcepts({
        APIInstance: self.APIInstance,
        courseData: courseData,
        conceptsModalTarget: document.querySelector('#stage'),
        conceptsCountTarget: document.querySelector('.learned-concepts')
    })
    
    preloadSpinner.updateMessage('Obteniendo información del usuario...')

    self.APIInstance.makeRequest({
        component: 'utils',
        method: 'getAllUsefulData',
        showFeedback: true,
        arguments: {
            shortName: courseData.shortName,
            unit: courseData.unit
        }
    })
    .then(function(response){
        self.initResponse = response
        preloadSpinner.updateMessage('Cargando imágenes...')
        return self.ovaPreload.fetchSources({
            sourceType: 'image',
            sources: imageSources.paths,
            stopOnReject: true,
            onProgress: function(args){
                preloadSpinner.updateMessage('Cargando recurso visual [' + _.size(args.loadedSources) + '/' + _.size(args.sources) + ']')
            },
            onFail: function(args){}
        }) 
    })
    .then(function(loadedImages){
        preloadSpinner.updateMessage('Cargando fuentes...')

        return self.ovaPreload.fetchSources({
            sourceType: 'font',
            sources: fontSources.families,
            stopOnReject: true,
            options: {
                timeout: 360000//6 min
            },
            onProgress: function(args){
                preloadSpinner.updateMessage('Cargando fuente [' + _.size(args.loadedSources) + '/' + _.size(args.sources) + ']')
            },
            onFail: function(args){}
        })
    })
    .then(function(response){
        self.ovaConcepts.props.concepts = _.pick(concepts, _.keys(self.initResponse.concepts.response))
        self.ovaConcepts.refreshConceptsCount()

        //Se inicializa la barra de progreso
        const finishedTopics = self.initResponse.finishedTopics.response
        if(_.isObject(finishedTopics) && _.size(finishedTopics)){
            self.ovaProgress.topics.finished = finishedTopics
        }

        self.ovaProgress.renderProgressBar({
            target: document.querySelector('.ova-content-header'),
            template: require('partials/progressBar.hbs')
        })

        self.ovaProgress.setProgressBarAmount()

        var slidesArguments = {}

        if(_.isObject(self.initResponse.courseActivities)){
            self.ovaCourseActivities = new ovaCourseActivities({
                data: self.initResponse.courseActivities.response,
                remoteURL: courseData.remoteURL
            })

            self.initResponse.courseActivities = {
                response: self.ovaCourseActivities.data
            }
        }

        _.forEach(self.initResponse, function(v, k){
            slidesArguments[k] = v.response
        })

        self.slidesConfig = _.merge({
            slidesArguments: slidesArguments,
            $target: presentation.getPresentationElement(),
            ovaProgress: self.ovaProgress,
            ovaConcepts: self.ovaConcepts,
        }, ovaConfig.slidesConfig)

        //Se requiere módulo que permite renderizar los templates estáticos almacenados en /partials  
        staticTemplates.renderSlides(self.slidesConfig)

        //Se inicializa la presentación
        presentation.init()

        //Se inicializan los botones de navegación
        ovaNavigationButtons.initializeNavigationButtons({
            $target: stageResize.currentProps.$stage
        })
        
        try{
            self.initResponse.team.response = JSON.parse(self.initResponse.team.response)
        }
        catch(error){
            console.error('Error while parsing team. Response:', self.initResponse.team.response)
            self.initResponse.team.response = {}
        }

        self.ovaTeam = new ovaTeam()
        self.ovaTeam.renderTeamTree({
            team: self.initResponse.team.response,
            target: document.querySelector('#stage'),
            classList: ['concealed'],
            treeName: 'Equipo de Diseño y Desarrollo',
        })

        self.ovaTeam.renderTeamTree({
            classList: ['static-team', 'concealed'],
            team: require('esapTeam.json'),
            target: document.querySelector('#stage'),
            treeName: 'Equipo ESAP',
        })

        self.ovaTeam.renderTeamIcon({
            target: document.querySelector('.ova-content-footer')
        })

        utils({
            ovaConfig: ovaConfig,
            ovaConcepts: self.ovaConcepts,
            ovaProgress: self.ovaProgress,
        })

        staticTemplates.addEventListeners({
            slidesConfig: self.slidesConfig,
            initResponse: self.initResponse,
            APIInstance: self.APIInstance,
            ovaProgress: self.ovaProgress,
            ovaConcepts: self.ovaConcepts,
        })

        //Se añaden los eventos que se desprenden
        //del cambio entre slides en diapositivas
        slideHooks.addHooks()

        preloadSpinner.hide()
        $('#wrapper').show()
        stageResize.resizeStage()

        self.ovaProgress.events.on('update', function(state){
            self.tippy.destroyAll()
            self.ovaProgress.updateVisualClue()
            self.tippy = ovaTooltip()
            if(_.size(state.pending()) === 0 && !self.finishedOva){
                finishOva()
                self.finishedOva = true
            }
        })

        if(_.size(self.ovaProgress.topics.pending()) === 0){
            finishOva()
            self.finishedOva = true
        }

        try{
            var addons = 'addOns.js'
            self.addOns = require(addons)
        }
        catch(error){}
    
        if(_.isFunction(self.addOns)){
            self.addOns()
        }

        try{
            var topicsFunctions = 'topicsFunctions.js'
            self.topicsFunctions = require(topicsFunctions)
        }
        catch(error){}
    
        if(_.isObject(self.topicsFunctions)){
            self.ovaTopicsFunctions =  new loTopicsFunctions({
                loProgress: self.ovaProgress,
                topicsFunctions: self.topicsFunctions
            })
        }

        self.tippy = ovaTooltip()
    })
    .catch(function(error){
        swal({
            type: 'error',
            title: 'Ocurrió un error al cargar el objeto de aprendizaje',
            text: 'Comprueba tu conexión a internet. Si el problema persiste, contacta a un administrador.'
        })

        console.error(error)
        
        preloadSpinner
            .updateMessage(error.message)
            .hideSpinner()
            .updateIcon(':(')
   
    })
}

module.exports = new app()
