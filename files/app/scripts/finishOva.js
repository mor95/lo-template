const swal = require('sweetalert2')
const _ = require('lodash')
const swalOptions = require('swalOptions.js')

module.exports = function () {
  var template = null;
  var customPath = 'templates/allDoneModal.hbs';
  var defaultPath = 'templates/allDoneDefaultModal.hbs';
  try{
    template = require(customPath)
  }
  catch(err){
    template = require(defaultPath)
  }

  swal(_.merge(swalOptions, {
    html: template(),
    customClass: 'ova-themed allDoneModal'
  })).then(function () {
    var $pdf = $(".pdf").removeClass("disabled"),
    $a = $pdf.find("a");
    $a.attr("href", $a.attr("_href"));
    $pdf.find("i").addClass("animated flip infinite")
    $pdf.on('click' , function(){
      $pdf.find("i").removeClass('infinite');
    })
  })
}
