/*
    Parámetros más frecuentes para modals de sweetalert.js
*/

const stageResize = require('stage-resize');
module.exports = {
    target: $('#modal-container').get(0),
    customClass: "ova-themed",
    showCloseButton: true,
    showConfirmButton: false,
    html: 'Some content here.'
}