const _ = require('lodash')
const getFilesRecursively = require('./getFilesRecursively.js')
const ignoreFiles = require('./ignoreFiles.js')
const fsExtra = require('fs-extra')
const fs = require('fs')
const chalk = require('chalk')
const log = console.log
const path = require('path')

module.exports = function(targetPath){
    if(typeof targetPath !== 'string'){
        throw new Error('A "targetPath" property was expected as a string.')
    }
    var filesPath = __dirname + '/files';
    var fileList = getFilesRecursively(filesPath);
    var ignoredFiles = [];
    _.forEach(fileList, function(v, k){
        /*var basename = path.basename(path.dirname(v));*/
        var _path = path.resolve('files/', v)
        _path = _path.split('files/')[1];
        ignoredFiles.push(_path);
        console.log(chalk`{green ${_path}}`)
    })

    fs.readdir(filesPath, function (err, files) {
        if (err) {
            return log(chalk`{red Unable to scan directory. ${err} }`)
        } 

        files.forEach(function (file) {
            var sourcePath = path.join(filesPath, file);
            var _targetPath = path.join(targetPath, file);
            var item = fs.lstatSync(sourcePath);
            fsExtra.copySync(sourcePath, _targetPath);
        });
        ignoreFiles(ignoredFiles, targetPath);
    });

}